from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models

# Create a connection to ElasticSearch

# connections.create_connection()
connections.create_connection(
    hosts=['https://elastic:AETUbOLk0DtdItcALBhakAkt@4b04821a955b4afdbcea028c36bb3c6e.ap-southeast-1.aws.found.io:9243'], timeout=20)

# ElasticSearch "model" mapping out what fields to index


class QuestionIndex(DocType):
    title = Text()
    timestamp = Date()
    content = Text()
    user = Text()
    pk = Text()

    class Index:
        name = 'question-index'


class AnswerIndex(DocType):
    user = Text(),
    content = Text()

    class Index:
        name = 'answer-index'


# Bulk indexing function, run in shell
def Question_bulk_indexing():
    # QuestionIndex.init()
    # es = Elasticsearch()
    # bulk(client=es, actions=(b.indexing() for b in models.Question.objects.all().iterator()))
    for b in models.Question.objects.all():
        b.indexing()




def Answer_bulk_indexing():
    AnswerIndex.init()
    es = Elasticsearch()
    bulk(client=es, actions=(b.indexing() for b in models.Answer.objects.all().iterator()))



# Simple search function
def search(query):
    s = Search(index='question-index').query("multi_match",query=query, fields=['title','content'])
    response = s.execute()
    pk_list = []
    for i in response:
        pk_list.append(i['pk'])
    return pk_list
