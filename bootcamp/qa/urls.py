from bootcamp.qa import views

from django.conf.urls import url


app_name = 'qa'
urlpatterns = [
    url(r'^$', views.QuestionListView.as_view(), name='index_noans'),
    url(r'^answered/$', views.QuestionAnsListView.as_view(), name='index_ans'),
    url(r'^indexed/$', views.QuestionsIndexListView.as_view(), name='index_all'),
    url(r'^question-detail/(?P<pk>\d+)/$', views.QuestionDetailView.as_view(), name='question_detail'),
    url(r'^ask-question/$', views.CreateQuestionView.as_view(), name='ask_question'),
    url(r'^propose-answer/(?P<question_id>\d+)/$', views.CreateAnswerView.as_view(), name='propose_answer'),
    url(r'^question/vote/$', views.question_vote, name='question_vote'),
    url(r'^answer/vote/$', views.answer_vote, name='answer_vote'),
    url(r'^accept-answer/$', views.accept_answer, name='accept_answer'),
    url(r'^update-answer/(?P<pk>[0-9a-f-]+)/$', views.UpdateAnswerView.as_view(), name='update_answer'),
    url(r'^update-question/(?P<pk>\d+)/$', views.UpdateQuestionView.as_view(), name='update_question'),
    url(r'^delete-answer/(?P<pk>[0-9a-f-]+)/$', views.DeleteAnswerView.as_view(), name='delete_answer'),
    url(r'^download-questions/$', views.download_user_question_request , name='download_questions'),
    url(r'^download-progress/$', views.get_download_progress , name='download-progress'),
    url(r'^download-question-file/$', views.download_questions_file , name='download-question-file'),

]
