from .models import Question, Answer
from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender=Question)
def index_post(sender, instance, **kwargs):
    instance.indexing()


@receiver(post_save, sender=Answer)
def index_post(sender, instance, **kwargs):
    instance.indexing()
