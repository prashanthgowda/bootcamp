from bootcamp.helpers import ajax_required
from bootcamp.qa.forms import QuestionForm
from bootcamp.qa.models import Answer, Question

from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.utils import IntegrityError
from django.http import JsonResponse, HttpResponse
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.http import require_http_methods
from django.views.generic import CreateView, DeleteView
from django.views.generic import DetailView, ListView, UpdateView
from bootcamp.qa.tasks import download_user_question
from config.settings import local
from celery.result import AsyncResult
import os


class QuestionsIndexListView(LoginRequiredMixin, ListView):
    """CBV to render a list view with all the registered questions."""
    model = Question
    paginate_by = 20
    context_object_name = "questions"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["popular_tags"] = Question.objects.get_counted_tags()
        context["active"] = "all"
        return context


class QuestionAnsListView(QuestionsIndexListView):
    """CBV to render a list view with all question which have
    been already marked as answered."""

    def get_queryset(self, **kwargs):
        return Question.objects.get_answered()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["active"] = "answered"
        return context


class QuestionListView(QuestionsIndexListView):
    """CBV to render a list view with all question which haven't been marked
    as answered."""

    def get_queryset(self, **kwargs):
        return Question.objects.get_unanswered()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["active"] = "unanswered"
        return context


class QuestionDetailView(LoginRequiredMixin, DetailView):
    """View to call a given Question object and to render all the details about
    that Question."""
    model = Question
    context_object_name = "question"


class CreateQuestionView(LoginRequiredMixin, CreateView):
    """
    View to handle the creation of a new question
    """
    form_class = QuestionForm
    template_name = "qa/question_form.html"
    message = _("Your question has been created.")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        return reverse("qa:index_noans")


class UpdateQuestionView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    """
    View to handle the creation of a new question
    """
    form_class = QuestionForm
    template_name = "qa/update_question_form.html"
    message = _("Your question has been updated.")
    model = Question

    def test_func(self):
        question = self.get_object()
        if self.request.user.id == question.user_id:
            return True
        else:
            return False

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        return reverse("qa:index_noans")


class DeleteQuestionView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    """
    View to handle the creation of a new question
    """
    form_class = QuestionForm
    template_name = "qa/delete_question_form.html"
    message = _("Your question has been deleted.")
    model = Question

    def test_func(self):
        question = get_object_or_404()
        if self.request.user.id == question.user_id:
            return True
        else:
            return False

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        return reverse("qa:index_noans")


class CreateAnswerView(LoginRequiredMixin, CreateView):
    """
    View to create new answers for a given question
    """
    model = Answer
    template_name = "qa/answer_form.html"
    fields = ["content", ]
    message = _("Thank you! Your answer has been posted.")

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.question_id = self.kwargs["question_id"]
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        return reverse(
            "qa:question_detail", kwargs={"pk": self.kwargs["question_id"]})


class UpdateAnswerView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    """
    View to create update answers for a given question
    """
    model = Answer
    template_name = "qa/answer_update_form.html"
    fields = ["content", ]
    message = _("Thank you! Your answer has been updated.")

    def test_func(self):
        answer = self.get_object()
        if self.request.user.id == answer.user_id:
            return True
        else:
            return False

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        answer = self.get_object()
        return reverse(
            "qa:question_detail", kwargs={"pk": answer.question_id})


class DeleteAnswerView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    """View to create update answers for a given question"""

    model = Answer
    template_name = "qa/delete_answer_form.html"
    message = _("Thank you! Your answer has been deleted.")

    def test_func(self):
        answer = get_object_or_404(Answer, uuid_id=self.kwargs["pk"])
        if self.request.user.id == answer.user_id:
            return True
        else:
            return False

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, self.message)
        answer = self.get_object()
        return reverse(
            "qa:question_detail", kwargs={"pk": answer.question_id})


@login_required
@ajax_required
@require_http_methods(["POST"])
def question_vote(request):
    """Function view to receive AJAX call, returns the count of votes
    a given question has recieved."""
    question_id = request.POST["question"]
    value = None
    if request.POST["value"] == "U":
        value = True

    else:
        value = False

    question = Question.objects.get(pk=question_id)
    try:
        question.votes.update_or_create(
            user=request.user, defaults={"value": value}, )
        question.count_votes()
        return JsonResponse({"votes": question.total_votes})

    except IntegrityError:  # pragma: no cover
        return JsonResponse({'status': 'false',
                             'message': _("Database integrity error.")},
                            status=500)


@login_required
@ajax_required
@require_http_methods(["POST"])
def answer_vote(request):
    """Function view to receive AJAX call, returns the count of votes
    a given answer has recieved."""
    answer_id = request.POST["answer"]
    value = None
    if request.POST["value"] == "U":
        value = True

    else:
        value = False

    answer = Answer.objects.get(uuid_id=answer_id)
    try:
        answer.votes.update_or_create(
            user=request.user, defaults={"value": value}, )
        answer.count_votes()
        return JsonResponse({"votes": answer.total_votes})

    except IntegrityError:  # pragma: no cover
        return JsonResponse({'status': 'false',
                             'message': _("Database integrity error.")},
                            status=500)


@login_required
@ajax_required
@require_http_methods(["POST"])
def accept_answer(request):
    """Function view to receive AJAX call, marks as accepted a given answer
    for an also provided question."""
    answer_id = request.POST["answer"]
    answer = Answer.objects.get(uuid_id=answer_id)
    answer.accept_answer()
    return JsonResponse({'status': 'true'}, status=200)


@login_required
@ajax_required
@require_http_methods(["GET"])
def download_user_question_request(request):
    """Function view to receive AJAX call, marks as accepted a given answer
    for an also provided question."""
    user_id = request.user.id
    result = download_user_question.delay(user_id)
    return JsonResponse({'task_id': result.id,
                         'task_status': result.status})


@login_required
@ajax_required
@require_http_methods(["GET"])
def get_download_progress(request):
    status = AsyncResult(request.GET["task_id"]).state
    return JsonResponse({'task_status': status})


@login_required
def download_questions_file(request):
    filename = str(request.user.id) + '.json'
    file_path = os.path.join(local.MEDIA_ROOT, filename)
    with open(file_path, 'rb') as fh:
        response = HttpResponse(fh.read(),
                                content_type="application/json",)
        response['Content-Disposition'] =\
            'attachment; filename=' + filename
        response['fileDownload'] = True
    return response
