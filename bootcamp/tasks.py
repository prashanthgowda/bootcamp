# # Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from bootcamp.qa.models import Question
from config.settings import local
import json
import os


@shared_task
def download_user_question(user_id):
    question_set = Question.objects.filter(user=user_id).values('title','content')
    question_list = []
    for question in question_set:
        question_list.append(question)
    filename = str(user_id) + '.json'
    filepath = os.path.join(local.MEDIA_ROOT, filename)
    with open(filepath, "w+") as f:
        json.dump(question_list, f)
