# Pull base image
FROM python:3.6-slim

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory to /app
WORKDIR /usr/src/app/bootcamp/

# Copy the current directory contents into the container at /app
COPY ./ /usr/src/app/bootcamp/

# Install any needed packages specified in requirements.txt
RUN apt-get update
RUN apt install -y libpq-dev python3-dev build-essential gunicorn
RUN pip3 install -r requirements.txt
# install dependencies
#RUN pip install --upgrade pip
#RUN pip install pipenv
#COPY ./Pipfile /usr/src/app/bootcamp/Pipfile
#RUN pipenv install --skip-lock --system --dev

# Make port 8000 available to the world outside this container
EXPOSE 8000

# copy entrypoint.sh
COPY ./entrypoint.sh /usr/src/app/bootcamp/entrypoint.sh

# run entrypoint.sh
ENTRYPOINT ["/usr/src/app/bootcamp/entrypoint.sh"]

